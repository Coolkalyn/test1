    var module = angular.module('app', ['ui.bootstrap','app.service']);


    module.controller('ContactController', function($scope, ContactService) {

      $scope.contacts = ContactService.list();



      $scope.currentPage = 0;
      $scope.pageSize = 6;
      $scope.data = ContactService.list();
      $scope.numberOfPages = function() {
        return Math.ceil($scope.data.length / $scope.pageSize);
      }

      $scope.nextPage = function() {
        if ($scope.currentPage < $scope.numberOfPages() - 1) {
          return $scope.currentPage++;
        }
      }

      $scope.previousPage = function() {
        if ($scope.currentPage > 0) {
          return $scope.currentPage--;
        }
      }

      $scope.add = function() {
        ContactService.add($scope.newcontact);
        $scope.newcontact = {};
      }


      $scope.save = function() {
        ContactService.save($scope.contactedit);
      }


      $scope.delete = function(id) {
        ContactService.delete(id);
      }


      $scope.edit = function(id) {
        $scope.contactedit = angular.copy(ContactService.get(id));
      }

    });


    module.filter('startFrom', function() {
      return function(input, start) {
        start = +start; //parse to int
        return input.slice(start);
      }
    });
