    var module = angular.module('app.service', ['ui.bootstrap']);


    module.service('ContactService', function() {


      var contacts = [{
        id: "1",
        'name': "Вася",
        'surname': "Пяточкин",
        'email': "jobmail1@gmail.com",
        'post': "Programmer",
        'workTime': "8",
        'phone': "5553331401"
      }, {
        id: "2",
        'name': "Петя",
        'surname': "Сидоров",
        'email': "jobmail2@gmail.com",
        'post': "Programmer",
        'workTime': "8",
        'phone': "5553331402"
      }, {
        id: "3",
        'name': "Саша",
        'surname': "Абрамович",
        'email': "jobmail3@gmail.com",
        'post': "Programmer",
        'workTime': "9",
        'phone': "5553331403"
      }, {
        id: "4",
        'name': "Вася",
        'surname': "Пушкин",
        'email': "jobmail4@gmail.com",
        'post': "Programmer",
        'workTime': "8",
        'phone': "5553331404"
      }, {
        id: "5",
        'name': "Степан",
        'surname': "Пяточкин",
        'email': "jobmail5@gmail.com",
        'post': "Programmer",
        'workTime': "10",
        'phone': "5553331405"
      }, {
        id: "6",
        'name': "Степан",
        'surname': "Сидоров",
        'email': "jobmail6@gmail.com",
        'post': "Programmer",
        'workTime': "10",
        'phone': "5553331406"
      }, {
        id: "7",
        'name': "Петя",
        'surname': "Пушкин",
        'email': "jobmail7@gmail.com",
        'post': "Analyst",
        'workTime': "7",
        'phone': "5553331407"
      }, {
        id: "8",
        'name': "Вася",
        'surname': "Ляшко",
        'email': "jobmail8@gmail.com",
        'post': "Analyst",
        'workTime': "8",
        'phone': "5553331408"
      }, {
        id: "9",
        'name': "Саша",
        'surname': "Пяточкин",
        'email': "jobmail9@gmail.com",
        'post': "Analyst",
        'workTime': "8",
        'phone': "5553331409"
      }, {
        id: "10",
        'name': "Петя",
        'surname': "Ляшко",
        'email': "jobmail10@gmail.com",
        'post': "Analyst",
        'workTime': "8",
        'phone': "5553331410"
      }, {
        id: "11",
        'name': "Вася",
        'surname': "Сидоров",
        'email': "jobmail11@gmail.com",
        'post': "Enginer",
        'workTime': "9",
        'phone': "5553331411"
      }, {
        id: "12",
        'name': "Саша",
        'surname': "Пяточкин",
        'email': "jobmail12@gmail.com",
        'post': "Enginer",
        'workTime': "8",
        'phone': "5553331412"
      }, {
        id: "13",
        'name': "Петя",
        'surname': "Абрамович",
        'email': "jobmail13@gmail.com",
        'post': "Enginer",
        'workTime': "10",
        'phone': "5553331413"
      }, {
        id: "14",
        'name': "Вася",
        'surname': "Абрамович",
        'email': "jobmail14@gmail.com",
        'post': "Architect",
        'workTime': "10",
        'phone': "5553331414"
      }, {
        id: "15",
        'name': "Саша",
        'surname': "Сидоров",
        'email': "jobmail15@gmail.com",
        'post': "Architect",
        'workTime': "7",
        'phone': "5553331415"
      }, {
        id: "16",
        'name': "Степан",
        'surname': "Абрамович",
        'email': "jobmail16@gmail.com",
        'post': "Architect",
        'workTime': "8",
        'phone': "555-333-1416"
      }];



      this.save = function(contact) {
        var k = 0;
        for (i in contacts) {
          if (contacts[i].email == contact.email && contact.id != contacts[i].id) {
            k++;
          }
        }
        if (contact.id != null && contact.name && contact.surname && contact.email && !isNaN(contact.workTime) && !isNaN(contact.phone) && (k == 0) && contact.post !== undefined) {
          for (i in contacts) {
            if (contacts[i].id == contact.id) {
              contacts[i] = contact;
            }
          }
        }
      }


      this.add = function(contact) {
        var k = 0;
        for (i in contacts) {
          if (contacts[i].email == contact.email) {
            k++;
          }
        }
        if (contact.name && contact.surname && contact.email && !isNaN(contact.workTime) && !isNaN(contact.phone) && (k == 0) && contact.post !== undefined) {
          function findId() {
            for (var newId = 1; newId < contacts.length; newId++) {
              var k = 0;
              for (i in contacts) {
                if (contacts[i].id == newId) {
                  k++;
                }
              }
              if (k == 0) {
                return newId;
              }
            }
            return ++newId;
          }
          contact.id = findId();
          contacts.push(contact);
        }
      }




      this.get = function(id) {
        for (i in contacts) {
          if (contacts[i].id == id) {
            return contacts[i];
          }
        }
      }


      this.delete = function(id) {
        for (i in contacts) {
          if (contacts[i].id == id) {
            contacts.splice(i, 1);
          }
        }
      }


      this.list = function() {
        return contacts;
      }
    });



 
